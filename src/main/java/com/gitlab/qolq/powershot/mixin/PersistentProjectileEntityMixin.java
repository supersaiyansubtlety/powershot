package com.gitlab.qolq.powershot.mixin;

import com.gitlab.qolq.powershot.Powerable;
import com.gitlab.qolq.powershot.Powershot;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.world.RayTraceContext;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PersistentProjectileEntity.class)
@SuppressWarnings("unused")
public abstract class PersistentProjectileEntityMixin extends Entity implements Powerable
{
    @Unique
    private static final String POWER_KEY = Powershot.MOD_ID + ":power";

    @Unique
    private float power = 0.0f;

    public PersistentProjectileEntityMixin(EntityType<?> type, World world)
    {
        super(type, world);
    }

    @Unique
    @Override
    public float getPower()
    {
        return power;
    }

    @Unique
    @Override
    public void setPower(float power)
    {
        this.power = power;
    }

    @Inject(method = "setVelocity", at = @At("HEAD"))
    private void setPower(double x, double y, double z, float speed, float inaccuracy, CallbackInfo info)
    {
        if (!world.isClient)
            //noinspection ConstantConditions
            setPower(Powershot.calculatePower((PersistentProjectileEntity)(Object)this, speed));
    }

    @Redirect(method = "tick", at = @At(value = "INVOKE", ordinal = 0, target =
        "Lnet/minecraft/world/World;rayTrace(Lnet/minecraft/world/RayTraceContext;)" +
        "Lnet/minecraft/util/hit/BlockHitResult;"
    ))
    private BlockHitResult trace(World world, RayTraceContext context)
    {
        //noinspection ConstantConditions
        return Powershot.trace(world, context, (PersistentProjectileEntity)(Object)this);
    }

    @Inject(method = "writeCustomDataToTag", at = @At("HEAD"))
    private void writeData(CompoundTag tag, CallbackInfo info)
    {
        tag.putFloat(POWER_KEY, getPower());
    }

    @Inject(method = "readCustomDataFromTag", at = @At("HEAD"))
    private void readData(CompoundTag tag, CallbackInfo info)
    {
        setPower(tag.getFloat(POWER_KEY));
    }
}
