package com.gitlab.qolq.powershot.mixin;

import net.minecraft.block.ShapeContext;
import net.minecraft.world.RayTraceContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(RayTraceContext.class)
public interface RayTraceContextAccessor
{
    @Accessor(value = "entityPosition")
    ShapeContext getEntityContext();
}
