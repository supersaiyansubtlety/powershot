package com.gitlab.qolq.powershot;

public interface Powerable
{
    float getPower();

    void setPower(float power);
}
