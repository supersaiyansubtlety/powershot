package com.gitlab.qolq.powershot;

import java.nio.file.Path;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.gitlab.qolq.powershot.mixin.RayTraceContextAccessor;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.RayTraceContext;
import net.minecraft.world.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Powershot implements ModInitializer
{
    public static final String MOD_ID = "powershot";

    static final Logger log = LogManager.getLogger(MOD_ID);

    private static Configuration config;

    @Override
    public void onInitialize()
    {
        Path dir = FabricLoader.getInstance().getConfigDirectory().toPath().resolve(MOD_ID);
        log.debug("Loading configs from {}", dir);
        config = new Configuration(dir);
        config.init();
    }

    public static float calculatePower(PersistentProjectileEntity arrow, float speed)
    {
        float power = config.basePower * speed;
        Entity shooter = arrow.getOwner();

        if (shooter instanceof LivingEntity)
        {
            LivingEntity livingShooter = (LivingEntity)shooter;
            // getActiveItem() will not work for crossbows
            ItemStack stack = livingShooter.getStackInHand(livingShooter.getActiveHand());

            if (!stack.isEmpty())
            {
                ListTag enchantments = stack.getEnchantments();

                for (int i = 0; i < enchantments.size(); i++)
                {
                    CompoundTag enchantment = enchantments.getCompound(i);
                    Identifier id = Identifier.tryParse(enchantment.getString("id"));

                    if (id != null && config.enchantmentToMultiplier.containsKey(id))
                    {
                        power += MathHelper.clamp(enchantment.getInt("lvl"), 0, 255) *
                                 config.enchantmentToMultiplier.getFloat(id);
                    }
                }
            }
        }

        return power;
    }

    public static BlockHitResult trace(World world, RayTraceContext context, PersistentProjectileEntity arrow)
    {
        if (world.isClient)
            return world.rayTrace(context);

        Powerable powerable = (Powerable)arrow;

        BiFunction<RayTraceContext, BlockPos, BlockHitResult> traceFn = (traceContext, position)->
        {
            Vec3d start = traceContext.getStart();
            Vec3d end = traceContext.getEnd();
            ShapeContext entityContext = ((RayTraceContextAccessor)traceContext).getEntityContext();
            BlockState state = world.getBlockState(position);
            VoxelShape shape = state.getCollisionShape(world, position, entityContext);
            BreakableBlockStateProperties properties = config.breakableStateToProperties.get(state);

            BlockHitResult result;

            if (properties == null)
            {
                result = world.rayTraceBlock(start, end, position, shape, state);
            }
            else
            {
                boolean breakable = powerable.getPower() >= properties.powerRequirement;

                if (breakable && shape.isEmpty()) // for blocks with no collision shapes (e.g. torches)
                    shape = state.getOutlineShape(world, position, entityContext);

                result = world.rayTraceBlock(start, end, position, shape, state);

                if (breakable && result != null)
                {
                    world.breakBlock(position, true, arrow.getOwner()); // destroy block + do drops
                    powerable.setPower(powerable.getPower() - properties.powerReduction);
                    result = null;
                }
            }

            if (result != null)
                powerable.setPower(0.0f);

            return result;
        };

        Function<RayTraceContext, BlockHitResult> missFn = traceContext->
        {
            Vec3d start = traceContext.getStart();
            Vec3d end = traceContext.getEnd();
            Vec3d dir = start.subtract(end);
            return BlockHitResult.createMissed(end, Direction.getFacing(dir.x, dir.y, dir.z), new BlockPos(start));
        };

        return BlockView.rayTrace(context, traceFn, missFn);
    }
}
