package com.gitlab.qolq.powershot;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.Property;
import net.minecraft.util.Identifier;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.registry.Registry;

import static com.gitlab.qolq.powershot.BreakableBlockStateProperties.DEFAULT_POWER_REDUCTION;
import static com.gitlab.qolq.powershot.BreakableBlockStateProperties.DEFAULT_POWER_REQUIREMENT;
import static com.gitlab.qolq.powershot.Powershot.log;

public final class StateToPropertyHashMapSerializer implements JsonDeserializer<StateToPropertyHashMap>,
                                                               JsonSerializer<StateToPropertyHashMap>
{
    private static final String BLOCKS_KEY = "blocks";
    private static final String BLOCK_ID_KEY = "id";
    private static final String BLOCK_STATES_KEY = "states";
    private static final String TAGS_KEY = "tags";
    private static final String POWER_REQUIREMENT_KEY = "power_req";
    private static final String POWER_REDUCTION_KEY = "power_red";
    private static final String NORMAL_BLOCK_STATE_ENTRY = "normal";

    /* ====================================== deserialize ====================================== */

    @Override
    public StateToPropertyHashMap deserialize(JsonElement source, Type type, JsonDeserializationContext context)
        throws JsonParseException
    {
        JsonArray jsonEntries = JsonHelper.asArray(source, "top-level element");
        StateToPropertyHashMap.Builder builder = new StateToPropertyHashMap.Builder();

        for (JsonElement jsonEntry : jsonEntries)
        {
            try
            {
                deserializeTopLevelEntry(JsonHelper.asObject(jsonEntry, "top-level entry"), builder);
            }
            catch (JsonParseException e)
            {
                log.warn("Error while parsing top-level entry", e);
            }
        }

        return builder.build();
    }

    /* ------------------------------------ top-level entry ------------------------------------ */

    private void deserializeTopLevelEntry(JsonObject jsonEntry, StateToPropertyHashMap.Builder builder)
    {
        JsonElement jsonBlocks = jsonEntry.get(BLOCKS_KEY);
        JsonElement jsonTags = jsonEntry.get(TAGS_KEY);

        if (jsonBlocks == null && jsonTags == null)
        {
            log.warn("Invalid top-level entry: no blocks or tags specified -> {}", jsonEntry);
            return;
        }

        BreakableBlockStateProperties properties = new BreakableBlockStateProperties(
            JsonHelper.getFloat(jsonEntry, POWER_REQUIREMENT_KEY, DEFAULT_POWER_REQUIREMENT),
            JsonHelper.getFloat(jsonEntry, POWER_REDUCTION_KEY, DEFAULT_POWER_REDUCTION)
        );

        if (jsonBlocks != null)
        {
            try
            {
                for (BlockState state : deserializeBlockEntries(jsonBlocks))
                    builder.put(state, properties);
            }
            catch (JsonParseException e)
            {
                log.warn("Error while parsing block entries", e);
            }
        }

        if (jsonTags != null)
        {
            try
            {
                for (Identifier tag : deserializeTagEntries(jsonTags))
                    builder.put(tag, properties);
            }
            catch (JsonParseException e)
            {
                log.warn("Error while parsing tag entries", e);
            }
        }
    }

    /* -------------------------------------- identifiers -------------------------------------- */

    private Identifier deserializeIdentifier(JsonElement json, String name)
    {
        try
        {
            return new Identifier(JsonHelper.asString(json, name));
        }
        catch (InvalidIdentifierException e)
        {
            throw new JsonParseException(e);
        }
    }

    /* --------------------------------------- tag entry --------------------------------------- */

    private Set<Identifier> deserializeTagEntries(JsonElement jsonTags)
    {
        if (jsonTags.isJsonArray())
        {
            Set<Identifier> tags = new HashSet<>();

            for (JsonElement jsonTag : jsonTags.getAsJsonArray())
            {
                try
                {
                    tags.add(deserializeIdentifier(jsonTag, "block tag entry"));
                }
                catch (JsonParseException e)
                {
                    log.warn("Error while parsing tag entry", e);
                }
            }

            return tags;
        }
        else
        {
            return Collections.singleton(deserializeIdentifier(jsonTags, "block tag entry"));
        }
    }

    /* -------------------------------------- block entry -------------------------------------- */

    private Set<BlockState> deserializeBlockEntries(JsonElement jsonBlocks)
    {
        if (jsonBlocks.isJsonObject())
        {
            return deserializeObjectBlockEntry(jsonBlocks.getAsJsonObject());
        }
        else if (jsonBlocks.isJsonArray())
        {
            return deserializeArrayBlockEntry(jsonBlocks.getAsJsonArray());
        }
        else if (jsonBlocks.isJsonPrimitive())
        {
            return deserializeStringBlockEntry(jsonBlocks.getAsJsonPrimitive());
        }

        log.warn("Invalid block entry: was not a string, an object, nor an array -> {}", jsonBlocks);
        return Collections.emptySet();
    }

    private Set<BlockState> deserializeArrayBlockEntry(JsonArray jsonBlocks)
    {
        Set<BlockState> states = new HashSet<>();

        for (JsonElement jsonBlock : jsonBlocks)
        {
            try
            {
                if (jsonBlock.isJsonObject())
                {
                    states.addAll(deserializeObjectBlockEntry(jsonBlock.getAsJsonObject()));
                }
                else if (jsonBlock.isJsonPrimitive())
                {
                    states.addAll(deserializeStringBlockEntry(jsonBlock.getAsJsonPrimitive()));
                }
                else
                {
                    log.warn("Invalid block entry: was neither a string nor an object -> {}", jsonBlock);
                }
            }
            catch (JsonParseException e)
            {
                log.warn("Error while parsing block entry", e);
            }
        }

        return states;
    }

    private Set<BlockState> deserializeStringBlockEntry(JsonPrimitive jsonBlock)
    {
        Identifier id = deserializeIdentifier(jsonBlock, "block entry");

        if (Registry.BLOCK.containsId(id))
        {
            return new HashSet<>(Registry.BLOCK.get(id).getStateManager().getStates());
        }
        else
        {
            log.warn("Invalid block entry: '{}' is not a registered block", id);
            return Collections.emptySet();
        }
    }

    private Set<BlockState> deserializeObjectBlockEntry(JsonObject jsonBlock)
    {
        JsonElement jsonId = jsonBlock.get(BLOCK_ID_KEY);

        if (jsonId == null)
        {
            log.warn("Invalid block entry: no block id specified -> {}", jsonBlock);
            return Collections.emptySet();
        }

        Identifier id = deserializeIdentifier(jsonId, "block id entry");

        if (!Registry.BLOCK.containsId(id))
        {
            log.warn("Invalid block entry: '{}' is not a registered block", id);
            return Collections.emptySet();
        }

        Block block = Registry.BLOCK.get(id);
        JsonElement jsonStates = jsonBlock.get(BLOCK_STATES_KEY);

        if (jsonStates == null)
        {
            return new HashSet<>(block.getStateManager().getStates());
        }
        else
        {
            StateManager<Block, BlockState> stateContainer = block.getStateManager();
            Set<BlockState> states = new HashSet<>();

            for (JsonElement jsonState : JsonHelper.asArray(jsonStates, "block states entry"))
            {
                try
                {
                    String stringState = JsonHelper.asString(jsonState, "block state entry");
                    BlockState state = deserializeBlockStateEntry(stringState, stateContainer, id);
                    if (state != null)
                        states.add(state);
                }
                catch (JsonParseException e)
                {
                    log.warn("Error while parsing '" + id + "' state entry -> " + jsonState, e);
                }
            }

            return states;
        }
    }

    /* ----------------------------------- block state entry ----------------------------------- */

    private <T extends Comparable<T>> BlockState deserializeBlockStateEntry(String stringState,
                                                                            StateManager<Block, BlockState> container,
                                                                            Identifier block)
    {
        if (NORMAL_BLOCK_STATE_ENTRY.equals(stringState))
            return container.getDefaultState();

        String[] stringProperties = stringState.split(",");
        BlockState state = container.getDefaultState();

        for (String stringProperty : stringProperties)
        {
            String[] propertyAndValueNames = stringProperty.split("=");
            if (propertyAndValueNames.length != 2)
            {
                log.warn("Invalid '{}' state entry: '{}' is not a property name and value pair", block, stringProperty);
                return null;
            }

            String propertyName = propertyAndValueNames[0];
            //noinspection unchecked
            Property<T> property = (Property<T>)container.getProperty(propertyName);
            if (property == null)
            {
                log.warn("Invalid '{}' state entry: no property named '{}'", block, propertyName);
                return null;
            }

            String valueName = propertyAndValueNames[1];
            T value = property.parse(valueName).orElse(null); // sigh
            if (value == null)
            {
                log.warn("Invalid '{}' state entry: property '{}' has no value '{}'", block, propertyName, valueName);
                return null;
            }

            state = state.with(property, value);
        }

        return state;
    }


    /* ======================================= serialize ======================================= */

    @Override
    public JsonElement serialize(StateToPropertyHashMap source, Type type, JsonSerializationContext context)
    {
        JsonArray jsonEntries = new JsonArray();

        Multimap<BreakableBlockStateProperties, BlockState> propsToStates = HashMultimap.create();
        Multimap<BreakableBlockStateProperties, Identifier> propsToTags = HashMultimap.create();

        // group states and tags which have the same properties so they can be compiled into one entry
        for (Entry<BlockState, BreakableBlockStateProperties> entry : source.states.entrySet())
            propsToStates.put(entry.getValue(), entry.getKey());
        for (Entry<Identifier, BreakableBlockStateProperties> entry : source.tags.entrySet())
            propsToTags.put(entry.getValue(), entry.getKey());

        Set<BreakableBlockStateProperties> properties = new HashSet<>();
        properties.addAll(propsToStates.keySet());
        properties.addAll(propsToTags.keySet());

        for (BreakableBlockStateProperties property : properties)
        {
            JsonArray jsonBlocks = null;
            JsonArray jsonTags = null;

            if (propsToStates.containsKey(property))
                jsonBlocks = serializeBlockEntry(propsToStates.get(property));
            if (propsToTags.containsKey(property))
                jsonTags = serializeTagEntry(propsToTags.get(property));

            jsonEntries.add(serializeTopLevelEntry(jsonBlocks, jsonTags, property));
        }

        return jsonEntries;
    }

    private JsonObject serializeTopLevelEntry(JsonArray jsonBlocks, JsonArray jsonTags,
                                              BreakableBlockStateProperties properties)
    {
        JsonObject jsonEntry = new JsonObject();

        if (jsonBlocks != null)
            jsonEntry.add(BLOCKS_KEY, jsonBlocks.size() == 1 ? jsonBlocks.get(0) : jsonBlocks);
        if (jsonTags != null)
            jsonEntry.add(TAGS_KEY, jsonTags.size() == 1 ? jsonTags.get(0) : jsonTags);
        if (properties.powerRequirement != DEFAULT_POWER_REQUIREMENT)
            jsonEntry.addProperty(POWER_REQUIREMENT_KEY, properties.powerRequirement);
        if (properties.powerReduction != DEFAULT_POWER_REDUCTION)
            jsonEntry.addProperty(POWER_REDUCTION_KEY, properties.powerReduction);

        return jsonEntry;
    }

    private JsonArray serializeTagEntry(Collection<Identifier> tags)
    {
        JsonArray jsonTags = new JsonArray();

        for (Identifier tag : tags)
            jsonTags.add(tag.toString());

        return jsonTags;
    }

    private JsonArray serializeBlockEntry(Collection<BlockState> states)
    {
        JsonArray jsonBlocks = new JsonArray();

        // group states which have the same parent block so they can be compiled into one json object
        Multimap<Identifier, BlockState> idToStates = HashMultimap.create();
        for (BlockState state : states)
            idToStates.put(Registry.BLOCK.getId(state.getBlock()), state);

        for (Entry<Identifier, Collection<BlockState>> entry : idToStates.asMap().entrySet())
        {
            Identifier id = entry.getKey();
            Block block = Registry.BLOCK.get(id);
            states = entry.getValue();

            if (states.containsAll(block.getStateManager().getStates()))
            {
                jsonBlocks.add(id.toString());
            }
            else
            {
                JsonArray stringStates = new JsonArray();
                for (BlockState state : states)
                    stringStates.add(serializeBlockStateEntry(state.getEntries()));

                JsonObject jsonBlock = new JsonObject();
                jsonBlock.addProperty(BLOCK_ID_KEY, id.toString());
                jsonBlock.add(BLOCK_STATES_KEY, stringStates);

                jsonBlocks.add(jsonBlock);
            }
        }

        return jsonBlocks;
    }

    @SuppressWarnings("unchecked")
    private <T extends Comparable<T>> JsonPrimitive serializeBlockStateEntry(Map<Property<?>, Comparable<?>> props)
    {
        StringBuilder builder = new StringBuilder();

        for (Entry<Property<?>, Comparable<?>> entry : props.entrySet())
        {
            if (builder.length() != 0)
                builder.append(",");

            Property<T> property = (Property<T>)entry.getKey();
            builder.append(property.getName())
                   .append("=")
                   .append(property.name((T)entry.getValue()));
        }

        if (builder.length() == 0)
            builder.append(NORMAL_BLOCK_STATE_ENTRY);

        return new JsonPrimitive(builder.toString());
    }
}
