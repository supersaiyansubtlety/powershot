package com.gitlab.qolq.powershot;

public final class BreakableBlockStateProperties
{
    public static final float DEFAULT_POWER_REQUIREMENT = 1.5f;
    public static final float DEFAULT_POWER_REDUCTION = 1.5f;
    public static final BreakableBlockStateProperties DEFAULT =
        new BreakableBlockStateProperties(DEFAULT_POWER_REQUIREMENT, DEFAULT_POWER_REDUCTION);

    public final float powerRequirement;
    public final float powerReduction;

    public BreakableBlockStateProperties(float powerRequirement, float powerReduction)
    {
        this.powerRequirement = Float.isNaN(powerRequirement) ? DEFAULT_POWER_REQUIREMENT : powerRequirement;
        this.powerReduction = Float.isNaN(powerReduction) ? DEFAULT_POWER_REDUCTION : powerReduction;
    }

    @Override
    public boolean equals(Object other)
    {
        if (this == other) return true;
        if (null == other) return false;
        if (getClass() != other.getClass()) return false;
        BreakableBlockStateProperties that = (BreakableBlockStateProperties)other;
        return powerRequirement == that.powerRequirement &&
               powerReduction == that.powerReduction;
    }

    @Override
    public int hashCode()
    {
        int hash = 17;
        hash = 37 * hash + Float.floatToIntBits(powerRequirement);
        hash = 37 * hash + Float.floatToIntBits(powerReduction);
        return hash;
    }

    @Override
    public String toString()
    {
        return String.format("[%.2f,%.2f]", powerRequirement, powerReduction);
    }
}
