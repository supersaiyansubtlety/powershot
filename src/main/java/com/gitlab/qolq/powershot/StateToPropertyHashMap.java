package com.gitlab.qolq.powershot;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tag.ServerTagManagerHolder;
import net.minecraft.tag.Tag;
import net.minecraft.tag.TagGroup;
import net.minecraft.util.Identifier;

public final class StateToPropertyHashMap
{
    private volatile Map<BlockState, BreakableBlockStateProperties> merged;

    final Map<BlockState, BreakableBlockStateProperties> states;
    final Map<Identifier, BreakableBlockStateProperties> tags;

    private StateToPropertyHashMap(Map<BlockState, BreakableBlockStateProperties> states,
                                   Map<Identifier, BreakableBlockStateProperties> tags)
    {
        this.states = Collections.unmodifiableMap(states);
        this.tags = Collections.unmodifiableMap(tags);
    }

    public BreakableBlockStateProperties get(BlockState key)
    {
        return merged.get(key);
    }

    public synchronized void reloadTags()
    {
        TagGroup<Block> collection = ServerTagManagerHolder.getTagManager().getBlocks();
        Map<BlockState, BreakableBlockStateProperties> map = new HashMap<>();

        for (Map.Entry<Identifier, BreakableBlockStateProperties> entry : tags.entrySet())
        {
            Tag<Block> tag = collection.getTag(entry.getKey());
            if (tag == null)
                continue;

            for (Block block : tag.values())
                for (BlockState state : block.getStateManager().getStates())
                    map.put(state, entry.getValue());
        }

        map.putAll(states);
        merged = Collections.unmodifiableMap(map);
    }

    public int statesSize()
    {
        return states.size();
    }

    public int tagsSize()
    {
        return tags.size();
    }

    public static final class Builder
    {
        private final Map<BlockState, BreakableBlockStateProperties> states = new HashMap<>();
        private final Map<Identifier, BreakableBlockStateProperties> tags = new LinkedHashMap<>();

        public void put(BlockState state, BreakableBlockStateProperties properties)
        {
            states.put(state, properties);
        }

        public void put(Block block, BreakableBlockStateProperties properties)
        {
            for (BlockState state : block.getStateManager().getStates())
                put(state, properties);
        }

        public void put(Identifier tag, BreakableBlockStateProperties properties)
        {
            tags.put(tag, properties);
        }

        public void put(Tag.Identified<Block> tag, BreakableBlockStateProperties properties)
        {
            put(tag.getId(), properties);
        }

        public StateToPropertyHashMap build()
        {
            return new StateToPropertyHashMap(states, tags);
        }
    }
}
