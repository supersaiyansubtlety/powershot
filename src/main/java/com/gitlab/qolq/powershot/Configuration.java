package com.gitlab.qolq.powershot;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import it.unimi.dsi.fastutil.objects.Object2FloatMap;
import it.unimi.dsi.fastutil.objects.Object2FloatMaps;
import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.ResourceReloadListenerKeys;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.resource.ResourceManager;
import net.minecraft.resource.ResourceType;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.Identifier;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.registry.Registry;
import org.apache.commons.io.FileUtils;

import static com.gitlab.qolq.powershot.Powershot.log;

public final class Configuration
{
    private static final int POWER_CALC_SCHEMA_VERSION = 2;
    private static final int BREAKABLE_BLOCKS_SCHEMA_VERSION = 2;

    private static final float DEFAULT_BASE_POWER = 1.0f;

    private static final String SCHEMA_VERSION_KEY = "schema_version";
    private static final String BASE_POWER_KEY = "base_power";
    private static final String ENCHANTMENT_MULTIPLIERS_KEY = "enchantment_multipliers";
    private static final String BREAKABLE_BLOCKS_KEY = "entries";

    private static final Gson gson = new GsonBuilder()
        .disableHtmlEscaping()
        .setPrettyPrinting()
        .registerTypeAdapter(StateToPropertyHashMap.class, new StateToPropertyHashMapSerializer())
        .create();

    public float basePower = DEFAULT_BASE_POWER;
    public Object2FloatMap<Identifier> enchantmentToMultiplier = getDefaultEnchantmentMultipliers();
    public StateToPropertyHashMap breakableStateToProperties = getDefaultBreakableStatesAndProperties();

    private final File powerCalculationConfigFile;
    private final File breakableBlocksConfigFile;

    public Configuration(Path dir)
    {
        powerCalculationConfigFile = dir.resolve("power_calculation.json").toFile();
        breakableBlocksConfigFile = dir.resolve("breakable_blocks.json").toFile();
    }

    public void init()
    {
        readPowerCalculationConfig();
        readBreakableBlocksConfig();

        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(
            new SimpleSynchronousResourceReloadListener()
            {
                private final Identifier id = new Identifier(Powershot.MOD_ID, "config");
                private final Collection<Identifier> deps = Collections.singleton(ResourceReloadListenerKeys.TAGS);

                @Override
                public Identifier getFabricId() { return id; }

                @Override
                public Collection<Identifier> getFabricDependencies() { return deps; }

                @Override
                public void apply(ResourceManager manager) { breakableStateToProperties.reloadTags(); }
            }
        );
    }

    public void readPowerCalculationConfig()
    {
        if (powerCalculationConfigFile.exists())
        {
            try
            {
                String json = FileUtils.readFileToString(powerCalculationConfigFile, StandardCharsets.UTF_8);
                JsonObject config = gson.fromJson(json, JsonObject.class);

                if (config.has(SCHEMA_VERSION_KEY))
                {
                    int schemaVersion = JsonHelper.getInt(config, SCHEMA_VERSION_KEY);

                    if (schemaVersion != POWER_CALC_SCHEMA_VERSION)
                    {
                        log.warn("Power calculation config has a different schema version (Expected: {}, Found: {})",
                                 POWER_CALC_SCHEMA_VERSION, schemaVersion);
                    }

                    basePower = JsonHelper.getFloat(config, BASE_POWER_KEY, DEFAULT_BASE_POWER);
                    enchantmentToMultiplier = readEnchantmentMultipliers(
                        JsonHelper.getObject(config, ENCHANTMENT_MULTIPLIERS_KEY, new JsonObject())
                    );
                }
                else
                {
                    log.warn("Power calculation config has no schema version. Assuming it's using the old schema");

                    Object2FloatMap<Identifier> map = new Object2FloatOpenHashMap<>(4);
                    map.put(new Identifier("power"), JsonHelper.getFloat(config, "power_multiplier", 1.0f));
                    map.put(new Identifier("piercing"), JsonHelper.getFloat(config, "piercing_multiplier", 1.0f));
                    map.put(new Identifier("punch"), JsonHelper.getFloat(config, "punch_multiplier", 1.0f));
                    map.put(new Identifier("knockback"), JsonHelper.getFloat(config, "knockback_multiplier", 1.0f));

                    basePower = JsonHelper.getFloat(config, BASE_POWER_KEY, DEFAULT_BASE_POWER);
                    enchantmentToMultiplier = Object2FloatMaps.unmodifiable(map);
                }
            }
            catch (IOException | JsonParseException e)
            {
                log.warn("Failed to read file " + powerCalculationConfigFile, e);
            }

            log.debug("Base power: {}", basePower);
            log.debug("Enchantment multipliers: {}", enchantmentToMultiplier.size());
        }
        else
        {
            writePowerCalculationConfig(); // create the file if it doesn't exist
        }
    }

    private Object2FloatMap<Identifier> readEnchantmentMultipliers(JsonObject jsonMultipliers)
    {
        Object2FloatMap<Identifier> multipliers = new Object2FloatOpenHashMap<>();

        for (Map.Entry<String, JsonElement> entry : jsonMultipliers.entrySet())
        {
            try
            {
                Identifier id = new Identifier(entry.getKey());

                if (Registry.ENCHANTMENT.containsId(id))
                {
                    float multiplier = JsonHelper.asFloat(entry.getValue(), id.toString());
                    if (multiplier != 0.0f && Float.isFinite(multiplier))
                        multipliers.put(id, multiplier);
                }
                else
                {
                    log.warn("Invalid enchantment multiplier entry: '{}' is not a registered enchantment", id);
                }
            }
            catch (JsonParseException | InvalidIdentifierException e)
            {
                log.warn("Error while parsing enchantment multiplier entry", e);
            }
        }

        return Object2FloatMaps.unmodifiable(multipliers);
    }

    public void readBreakableBlocksConfig()
    {
        if (breakableBlocksConfigFile.exists())
        {
            try
            {
                String json = FileUtils.readFileToString(breakableBlocksConfigFile, StandardCharsets.UTF_8);
                JsonElement element = gson.fromJson(json, JsonElement.class);

                if (element.isJsonObject())
                {
                    JsonObject config = element.getAsJsonObject();
                    int schemaVersion = JsonHelper.getInt(config, SCHEMA_VERSION_KEY);

                    if (schemaVersion != BREAKABLE_BLOCKS_SCHEMA_VERSION)
                    {
                        log.warn("Breakable blocks config has a different schema version (Expected: {}, Found: {})",
                                 BREAKABLE_BLOCKS_SCHEMA_VERSION, schemaVersion);
                    }

                    breakableStateToProperties = gson.fromJson(
                        JsonHelper.getArray(config, BREAKABLE_BLOCKS_KEY, new JsonArray()),
                        StateToPropertyHashMap.class
                    );
                }
                else
                {
                    log.warn("Breakable blocks config does not contain an object. Assuming it's using the old schema");
                    breakableStateToProperties = gson.fromJson(element, StateToPropertyHashMap.class);
                }
            }
            catch (IOException | JsonParseException e)
            {
                log.warn("Failed to read file " + breakableBlocksConfigFile, e);
            }

            log.debug("Breakable block states: {}", breakableStateToProperties.statesSize());
            log.debug("Breakable block tags: {}", breakableStateToProperties.tagsSize());
        }
        else
        {
            writeBreakableBlocksConfig(); // create the file if it doesn't exist
        }
    }

    public void writePowerCalculationConfig()
    {
        try
        {
            JsonObject config = new JsonObject();
            JsonObject multipliers = new JsonObject();

            for (Object2FloatMap.Entry<Identifier> entry : enchantmentToMultiplier.object2FloatEntrySet())
            {
                float multiplier = entry.getFloatValue();
                if (multiplier != 0.0f && Float.isFinite(multiplier))
                    multipliers.addProperty(entry.getKey().toString(), multiplier);
            }

            config.addProperty(SCHEMA_VERSION_KEY, POWER_CALC_SCHEMA_VERSION);
            config.addProperty(BASE_POWER_KEY, basePower);
            config.add(ENCHANTMENT_MULTIPLIERS_KEY, multipliers);

            FileUtils.write(powerCalculationConfigFile, gson.toJson(config), StandardCharsets.UTF_8, false);
        }
        catch (IOException e)
        {
            log.warn("Failed to write file " + powerCalculationConfigFile, e);
        }
    }

    public void writeBreakableBlocksConfig()
    {
        try
        {
            JsonObject config = new JsonObject();

            config.addProperty(SCHEMA_VERSION_KEY, BREAKABLE_BLOCKS_SCHEMA_VERSION);
            config.add(BREAKABLE_BLOCKS_KEY, gson.toJsonTree(breakableStateToProperties, StateToPropertyHashMap.class));

            FileUtils.write(breakableBlocksConfigFile, gson.toJson(config), StandardCharsets.UTF_8, false);
        }
        catch (IOException e)
        {
            log.warn("Failed to write file " + breakableBlocksConfigFile, e);
        }
    }

    public static Object2FloatMap<Identifier> getDefaultEnchantmentMultipliers()
    {
        Object2FloatMap<Identifier> defaults = new Object2FloatOpenHashMap<>(4);

        defaults.put(Registry.ENCHANTMENT.getId(Enchantments.POWER), 1.0f);
        defaults.put(Registry.ENCHANTMENT.getId(Enchantments.PIERCING), 1.0f);
        defaults.put(Registry.ENCHANTMENT.getId(Enchantments.PUNCH), 1.0f);
        defaults.put(Registry.ENCHANTMENT.getId(Enchantments.KNOCKBACK), 1.0f);

        return Object2FloatMaps.unmodifiable(defaults);
    }

    public static StateToPropertyHashMap getDefaultBreakableStatesAndProperties()
    {
        StateToPropertyHashMap.Builder defaults = new StateToPropertyHashMap.Builder();

        defaults.put(Blocks.BLUE_ICE, new BreakableBlockStateProperties(5.0f, 5.0f));
        defaults.put(Blocks.PACKED_ICE, new BreakableBlockStateProperties(4.0f, 4.0f));

        BreakableBlockStateProperties properties = new BreakableBlockStateProperties(3.15f, 3.15f);
        defaults.put(Blocks.GLASS, properties);
        defaults.put(Blocks.WHITE_STAINED_GLASS, properties);
        defaults.put(Blocks.ORANGE_STAINED_GLASS, properties);
        defaults.put(Blocks.MAGENTA_STAINED_GLASS, properties);
        defaults.put(Blocks.LIGHT_BLUE_STAINED_GLASS, properties);
        defaults.put(Blocks.YELLOW_STAINED_GLASS, properties);
        defaults.put(Blocks.LIME_STAINED_GLASS, properties);
        defaults.put(Blocks.PINK_STAINED_GLASS, properties);
        defaults.put(Blocks.GRAY_STAINED_GLASS, properties);
        defaults.put(Blocks.LIGHT_GRAY_STAINED_GLASS, properties);
        defaults.put(Blocks.CYAN_STAINED_GLASS, properties);
        defaults.put(Blocks.PURPLE_STAINED_GLASS, properties);
        defaults.put(Blocks.BLUE_STAINED_GLASS, properties);
        defaults.put(Blocks.BROWN_STAINED_GLASS, properties);
        defaults.put(Blocks.GREEN_STAINED_GLASS, properties);
        defaults.put(Blocks.RED_STAINED_GLASS, properties);
        defaults.put(Blocks.BLACK_STAINED_GLASS, properties);
        defaults.put(Blocks.ICE, properties);
        defaults.put(Blocks.FROSTED_ICE, properties);
        defaults.put(Blocks.MELON, properties);
        defaults.put(Blocks.PUMPKIN, properties);
        defaults.put(Blocks.CARVED_PUMPKIN, properties);

        properties = new BreakableBlockStateProperties(2.0f, 2.0f);
        defaults.put(BlockTags.FLOWER_POTS, properties);
        defaults.put(Blocks.GLASS_PANE, properties);
        defaults.put(Blocks.WHITE_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.ORANGE_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.MAGENTA_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.LIGHT_BLUE_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.YELLOW_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.LIME_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.PINK_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.GRAY_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.LIGHT_GRAY_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.CYAN_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.PURPLE_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.BLUE_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.BROWN_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.GREEN_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.RED_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.BLACK_STAINED_GLASS_PANE, properties);
        defaults.put(Blocks.END_ROD, properties);
        defaults.put(Blocks.LANTERN, properties);
        defaults.put(Blocks.SOUL_LANTERN, properties);
        defaults.put(Blocks.TRIPWIRE_HOOK, properties);
        defaults.put(Blocks.BAMBOO, properties);

        properties = BreakableBlockStateProperties.DEFAULT;
        defaults.put(Blocks.TORCH, properties);
        defaults.put(Blocks.WALL_TORCH, properties);
        defaults.put(Blocks.REDSTONE_TORCH, properties);
        defaults.put(Blocks.REDSTONE_WALL_TORCH, properties);
        defaults.put(Blocks.SOUL_TORCH, properties);
        defaults.put(Blocks.SOUL_WALL_TORCH, properties);
        defaults.put(Blocks.COCOA, properties);
        defaults.put(Blocks.SEA_PICKLE, properties);

        defaults.put(Blocks.LILY_PAD, new BreakableBlockStateProperties(1.0f, 1.0f));

        return defaults.build();
    }
}
