![Powershot](src/main/resources/logo.png)

_A Minecraft mod allowing projectiles to break blocks_

- Homepage: <https://www.gitlab.com/qolq/powershot/wikis>
- Sources: <https://www.gitlab.com/qolq/powershot>
- Issue Tracker: <https://www.gitlab.com/qolq/powershot/issues>

---

Copyright (c) 2020 qolq

The software included in Powershot is free to redistribute and/or
modify under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

The artworks included in Powershot are free to redistribute and/or
modify under the terms of the Creative Commons
Attribution-ShareAlike 4.0 International License.

Powershot is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received copies of the licenses along with Powershot.
If not, see <https://www.gnu.org/licenses/> and
<https://www.creativecommons.org/licenses/by-sa/4.0/>
