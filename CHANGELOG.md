# Change Log
All notable changes to this project will be documented in this file.

## [1.1.0-1.16.2-fabric] - 2020-08-12
### Changed
- Updated to Minecraft 1.16.2

## [1.1.0-1.16.1-fabric] - 2020-07-29
### Fixed
- Occasional crashing when config files are invalid

### Changed
- Config file formats

### Added
- Option to specify multipliers for arbitrary enchantments in config
- Default breakable block config entries for
  - `minecraft:soul_lantern`
  - `minecraft:soul_torch`
  - `minecraft:soul_wall_torch`

## [1.0.0-1.16.1-fabric] - 2020-07-08
### Changed
- Updated to Minecraft 1.16.1

## [1.0.0-1.15.2-fabric] - 2020-07-06
### Fixed
- Breakable blocks config _object block entries_ being serialized as
  _top-level entries_
- Crashing when breakable blocks config has invalid entries

### Changed
- Config file name from `general.json` to `power_calculation.json`
- Breakable blocks config to not accept _string top-level entries_
- Default breakable blocks config values

### Added
- Powershot logo and icon
- Block tag support for breakable block config file

## [0.1.0] - 2020-06-10
### Added
- Block-breaking behavior to arrows
  - Arrow power level which decreases with each block broken
  - Config file to specify breakable blocks and their properties
